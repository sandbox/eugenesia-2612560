This module sets up Avanser call tracking on your website.


Below is the technical document from Avanser detailing how to set up tracking
on a normal website.

------

Web Call Tracking Set-up Guide

This document will guide you through the process of implementing AVANSER's
dynamic web call tracking numbers.

Remember that Google Analytics tracking code must be installed throughout your
website. The code should be placed immediately before the closing </head> tag.

AVANSER's javascript code will automatically identify the source of the visitor
and replace your regular phone number with the corresponding web call tracking
number.

Step 1: Copy and paste the following javascript code on each web page where you
are promoting your phone number. This code should be inserted immediately
before the closing </BODY> tag. 

<script type="text/javascript"> 
  Math.ClientId=6509;
  Math.trackingcode=10;
  var gaJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
  document.write(unescape("%3Cscript src='" + gaJsHost + "analytics.avanser.com.au/service/webtracker.js' type='text/javascript'%3E%3C/script%3E"));
</script>

---

Step 2: Define the following span class and replace "your number" with your
original (non-dynamic) phone number. 

For example: <span class="AVANSERnumber"> 1300 123 456</span> 

<span class="AVANSERnumber">your number</span>

------

