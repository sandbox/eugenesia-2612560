<?php
/**
 * @file
 * Avanser call tracking code template.
 */
?>

<!-- Avanser call tracking -->
<script type="text/javascript"> 
  Math.ClientId = <?php print $avanser_client_id; ?>;
  Math.trackingcode = <?php print $avanser_tracking_code; ?>;
  var gaJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
  document.write(unescape("%3Cscript src='" + gaJsHost
    + "<?php print $avanser_domain; ?>/service/webtracker.js' type='text/javascript'%3E%3C/script%3E"));
</script>

